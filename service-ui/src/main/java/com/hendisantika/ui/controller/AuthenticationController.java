package com.hendisantika.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hendisantika.ui.api.UserManagementServiceAPI;
import com.hendisantika.ui.dto.AuthTokenDTO;
import com.hendisantika.ui.dto.AuthenticationDTO;

/**
 * Created by IntelliJ IDEA.
 * Project : candidate-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/17
 * Time: 06.32
 * To change this template use File | Settings | File Templates.
 */


@RestController
public class AuthenticationController {

	@Autowired
	private UserManagementServiceAPI userManagementServiceAPI;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public AuthTokenDTO authenticate(@RequestBody AuthenticationDTO authenticationDTO) {
		// Authenticate the user
		AuthTokenDTO authToken = userManagementServiceAPI.authenticateUser(authenticationDTO);
		// TODO If authentication fails, return an unauthorized error code

		return authToken;
	}
}
