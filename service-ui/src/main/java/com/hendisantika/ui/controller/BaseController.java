package com.hendisantika.ui.controller;

import com.hendisantika.common.security.JsonWebTokenAuthentication;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by IntelliJ IDEA.
 * Project : candidate-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/17
 * Time: 06.35
 * To change this template use File | Settings | File Templates.
 */


public class BaseController {

	protected String getAuthorizationToken() {
		String token = null;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication.getClass().isAssignableFrom(JsonWebTokenAuthentication.class)) {
			JsonWebTokenAuthentication jwtAuthentication = (JsonWebTokenAuthentication) authentication;
			token = jwtAuthentication.getJsonWebToken();
		}
		return token;
	}
}
