package com.hendisantika.ui.controller;

import com.hendisantika.common.security.JsonWebTokenAuthentication;
import com.hendisantika.ui.api.UserManagementServiceAPI;
import com.hendisantika.ui.dto.RoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * Project : candidate-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/17
 * Time: 06.37
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/api/roles")
public class RoleController extends BaseController {

	@Autowired
	private UserManagementServiceAPI userManagementAPI;

	@RequestMapping(method = RequestMethod.POST)
	public RoleDTO createRole(@RequestBody RoleDTO role) {
		return userManagementAPI.createRole(getAuthorizationToken(), role);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteRole(@PathVariable int id) {
		userManagementAPI.deleteRole(getAuthorizationToken(), id);
	}

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<RoleDTO> findAllRoles(@AuthenticationPrincipal JsonWebTokenAuthentication jwt) {
		return userManagementAPI.findAllRoles(getAuthorizationToken());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public RoleDTO findRoleById(@PathVariable int id) {
		return userManagementAPI.findRoleById(getAuthorizationToken(), id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void updateRole(@PathVariable int id, @RequestBody RoleDTO role) {
		userManagementAPI.updateRole(getAuthorizationToken(), id, role);
	}
}
