package com.hendisantika.ui.dto;
/**
 * Created by IntelliJ IDEA.
 * Project : candidate-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/17
 * Time: 06.22
 * To change this template use File | Settings | File Templates.
 */

public class RoleDTO {
	public int id;
	public String name;

	public RoleDTO() {
	}

	public RoleDTO(int id, String name) {
		this.id = id;
		this.name = name;
	}
}
