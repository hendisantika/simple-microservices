package com.hendisantika.usermanagement.controller;

import com.hendisantika.usermanagement.dto.RoleDTO;
import com.hendisantika.usermanagement.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : candidate-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/17
 * Time: 08.30
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/roles")
public class RoleController {

	@Autowired
	private UserManagementService userManagementService;

	@RequestMapping(method = RequestMethod.POST)
	public RoleDTO createRole(@RequestBody RoleDTO roleDTO) {
		return userManagementService.createRole(roleDTO);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteRole(@PathVariable int id) {
		userManagementService.deleteRole(id);
	}

	@RequestMapping(method = RequestMethod.GET)
	public Collection<RoleDTO> findAllRoles() {
		return userManagementService.findAllRoles();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public RoleDTO findRoleById(@PathVariable int id) {
		return userManagementService.findRoleById(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void updateRole(@PathVariable int id, @RequestBody RoleDTO roleDTO) {
		roleDTO = userManagementService.updateRole(id, roleDTO);
	}

}
