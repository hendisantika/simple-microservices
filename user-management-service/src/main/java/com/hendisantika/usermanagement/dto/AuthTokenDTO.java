package com.hendisantika.usermanagement.dto;

/**
 * Created by IntelliJ IDEA.
 * Project : candidate-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/17
 * Time: 08.13
 * To change this template use File | Settings | File Templates.
 */

// TODO Add fields to this class that the client can use, such as userId, email, roles
public class AuthTokenDTO {
	public String token;
}
