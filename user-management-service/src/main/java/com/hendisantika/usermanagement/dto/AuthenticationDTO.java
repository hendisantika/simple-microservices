package com.hendisantika.usermanagement.dto;

/**
 * Created by IntelliJ IDEA.
 * Project : candidate-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/17
 * Time: 08.14
 * To change this template use File | Settings | File Templates.
 */

public class AuthenticationDTO {
	public String email;
	public String password;
}
