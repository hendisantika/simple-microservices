package com.hendisantika.usermanagement.dto;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : candidate-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/17
 * Time: 08.12
 * To change this template use File | Settings | File Templates.
 */

public class UserDTO {
	public int id;
	public String email;
	public String password;
	public Collection<Integer> roleIds;

	public UserDTO() {
	}

	public UserDTO(int id, String email, Collection<Integer> roleIds) {
		this.id = id;
		this.email = email;
		this.roleIds = roleIds;
	}

	public UserDTO(String email, String password, Collection<Integer> roleIds) {
		this.email = email;
		this.password = password;
		this.roleIds = roleIds;
	}
}
