package com.hendisantika.usermanagement.repository;

import com.hendisantika.usermanagement.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : candidate-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/11/17
 * Time: 08.15
 * To change this template use File | Settings | File Templates.
 */

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
}
